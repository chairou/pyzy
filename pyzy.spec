Summary:    The Chinese PinYin and Bopomofo conversion library
Name:       pyzy
Version:    0.1.0
Release:    1%{?dist}
License:    LGPL-2.1-or-later
URL:        https://github.com/pyzy/pyzy
Source0:    http://pyzy.googlecode.com/files/%{name}-%{version}.tar.gz
Source1:    http://pyzy.googlecode.com/files/pyzy-database-1.0.0.tar.bz2
Patch0000:     pyzy-0.1.0-fixes-compile.patch
Patch0001:     pyzy-0.1.0-port-to-python3.patch

BuildRequires:  make, gcc-c++, glib2-devel, libtool, pkgconfig, sqlite-devel, libuuid-devel, python3

Requires:   pyzy-db = %{version}-%{release}
Provides:   ibus-pinyin-db-android
Provides:   ibus-pinyin-db-open-phrase

%description
The Chinese Pinyin and Bopomofo conversion library.

%package    devel
Summary:    Development tools for pyzy
Requires:   %{name} = %{version}-%{release}
Requires:   glib2-devel

%description devel
The pyzy-devel package contains the header files for pyzy.

%package    db-open-phrase
Summary:    The open phrase database for pyzy
BuildArch:  noarch
Provides:   pyzy-db

%description db-open-phrase
The phrase database for pyzy from open-phrase project.

%package    db-android
Summary:    The android phrase database for pyzy
BuildArch:  noarch
Provides:   pyzy-db

%description db-android
The phrase database for pyzy from android project.

%prep
%autosetup -p1
cp -p %{SOURCE1} data/db/open-phrase

%build
%configure --disable-static --enable-db-open-phrase
%make_build

%install
%make_install
rm -f %{buildroot}%{_libdir}/*.la

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license COPYING
%doc AUTHORS README
%{_libdir}/lib*.so.*
%{_datadir}/pyzy/phrases.txt
%{_datadir}/pyzy/db/create_index.sql
%dir %{_datadir}/pyzy
%dir %{_datadir}/pyzy/db

%files devel
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/*
%{_includedir}/*

%files db-open-phrase
%{_datadir}/pyzy/db/open-phrase.db

%files db-android
%{_datadir}/pyzy/db/android.db

%changelog
* Thu Aug 31 2023 Chair <chairou@tencent.com> - 0.1.0-1
- initial for https://github.com/pyzy/pyzy
